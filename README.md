## Tecnologías 

* sprint boot v2.1.5.RELEASE
* java 1.8
* mysql 8.0.17

# Lanzamiento

1. Crear Base datos : 

Nombre:test1

Host:mysql://localhost:3306/test1

2. ejecutar script :

link > https://gitlab.com/diogoalfa/auth-server/-/blob/master/doc/db/script-inicial.sql

3. ejecutar en consola : mvn spring-boot:run


### Endpoint

1. Cambiar password:

* http://localhost:8081/auth/user/password
* method: POST
* Bearer : Obtener token de proyecto https://gitlab.com/diogoalfa/auth-server

* BodyRequest: application/json
```
{
    "username":"diego",
    "password":"clave123",
    "newPassword":"diego123"
}
```