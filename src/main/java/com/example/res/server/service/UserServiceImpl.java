package com.example.res.server.service;

import com.example.res.server.exception.UserException;
import com.example.res.server.model.User;
import com.example.res.server.model.UserAuth;
import com.example.res.server.model.UserDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * @author Diego Navia <diego.navia@icloud.com>
 * @file-description :
 */
@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);



    @Autowired
    UserDao userDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public User getUser(String username){
        LOGGER.debug("getUser");
        return userDao.findByUsername(username);
    }

    @Override
    public boolean changePass(UserAuth userAuth) {
        User user = userDao.findByUsername(userAuth.getUsername());
        LOGGER.debug("changePass user:{}",user);
        if (passwordEncoder
                .matches(
                        userAuth.getPassword(),
                        user.getPassword()
                )
        ) {
            userDao.updatePasswordUser(
                                userAuth.getUsername(),
                                passwordEncoder
                                        .encode(userAuth.getNewPassword())
                        );
            return true;
        } else {
            throw new UserException("Password incorrecta");
        }
    }
}
