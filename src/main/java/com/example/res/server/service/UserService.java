package com.example.res.server.service;

import com.example.res.server.model.User;
import com.example.res.server.model.UserAuth;

/**
 * @author Diego Navia <diego.navia@icloud.com>
 * @file-description :
 */
public interface UserService {

    /**
     * Get User
     * @param username
     * @return
     */
    public User getUser(String username);

    /**
     * Change Password
     * @param userAuth
     * @return
     */
    public boolean changePass(UserAuth userAuth);
}
