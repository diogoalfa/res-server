package com.example.res.server;

import com.example.res.server.model.JsonResponse;
import com.example.res.server.model.User;
import com.example.res.server.model.UserAuth;
import com.example.res.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author Diego Navia <diego.navia@icloud.com>
 * @file-description :
 */
@RestController
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("user")
    public User getUser(@RequestParam("username") String username){
        return userService.getUser(username);
    }

    @PostMapping("/user/password")
    public ResponseEntity changePassword(@RequestBody UserAuth userAuth){
        userService.changePass(userAuth);
        return ResponseEntity.ok(new JsonResponse(HttpStatus.OK,"ok"));
    }

}
