package com.example.res.server.exception;

/**
 * @author Diego Navia <diego.navia@icloud.com>
 * @file-description :
 */
public class UserException extends RuntimeException {

    public UserException(String message) {
        super(message);
    }
}
