package com.example.res.server.model;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * @author Diego Navia <diego.navia@icloud.com>
 * @file-description :
 */
@Repository
public interface UserDao extends CrudRepository<User, Long> {

    @Query("select u from User u where u.username = ?1")
    User findByUsername(String username);

    @Query("select u from User u where u.clientId = ?1")
    User findByUserClientId(String clientId);

    @Query("select u from User u where u.username = ?1 and u.password= ?2")
    User findUserByUsernameAndPassword(String username, String pass);

    @Modifying
    @Transactional
    @Query("update User u set u.password = ?2 where u.username = ?1")
    void updatePasswordUser(
            @Param("username") String username,
            @Param("password")  String newPass);

}
