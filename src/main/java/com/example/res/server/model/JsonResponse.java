package com.example.res.server.model;

import org.springframework.http.HttpStatus;

import java.io.Serializable;

/**
 * @author Diego Navia <diego.navia@icloud.com>
 * @file-description : Objeto response para los endpoint
 */
public class JsonResponse implements Serializable {

    private String status;
    private int codeStatus;
    private Object data;

    public JsonResponse(String status, int codeStatus, Object data) {
        this.status = status;
        this.codeStatus = codeStatus;
        this.data = data;
    }

    public JsonResponse(HttpStatus httpStatus, Object data) {
        this.status = httpStatus.getReasonPhrase();
        this.codeStatus = httpStatus.value();
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCodeStatus() {
        return codeStatus;
    }

    public void setCodeStatus(int codeStatus) {
        this.codeStatus = codeStatus;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
